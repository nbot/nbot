%% ------------------------------------------------------------------------------
%% "THE BEER-WARE LICENSE" (Revision 42):
%% <meskio@sindominio.net> wrote this file. As long as you retain this notice you
%% can do whatever you want with this stuff. If we meet some day, and you think
%% this stuff is worth it, you can buy me a beer in return Ruben Pollan
%% ------------------------------------------------------------------------------

-module(exec).
-export([start/0, exec/1]).
-include_lib("records.hrl").

start() ->
    Pid = spawn_link(fun exec/0),
    register(exec, Pid).

%% @spec exec([atom()]) -> .
exec(Request) ->
    exec ! Request.

getBoard() ->
    receive
	['BoardSize', '0', Height, Width] ->
	    H = atom_to_integer(Height),
	    W = atom_to_integer(Width),
	    board:gen(H, W)
    end.

% Init commands
init() ->
    receive
	['Version',Num] when Num =/= '1' ->
	    throw({protocolVersionErr, "Version number not supported"});
	['BoardSize', '0', Height, Width] ->
	    init(),
	    H = atom_to_integer(Height),
	    W = atom_to_integer(Width),
	    board:gen(H, W);
	['BeginGame'] ->
	    speak:speak(['Message', 'Staring the game.']);
	_ ->
	    init()
    end.

% On game commands
loop(#piece{} = P, Board) ->
    receive
	['Exit'] ->
	    init:stop();
	['NewPiece', Num] ->
	    loop(P#piece{num=Num, type=null}, Board);
	['RowUpdate', '0', Row | Cols] ->
	    R = atom_to_integer(Row),
	    B = board:modify(Board, R, Cols),
	    if (P#piece.type =:= null) ->
		Piece = board:detectPiece(Cols),
		if (Piece =/= null) ->
		    NewPiece = P#piece{type=Piece, new=true},
		    loop(NewPiece, B);
		true -> none
		end;
	    true -> none
	    end,
	    loop(P, B);
	% use the stamp to calculate and move, so the board is fully updated
	['TimeStamp', _] ->
	    if (P#piece.new =:= true) ->
		{Move, _} = calculate:rpc(P#piece.type, Board),
		move:move([Move, P]),
		loop(P#piece{new=false}, Board);
	    true -> none
	    end,
	    loop(P, Board);
	_ -> %ignore the rest of the commands
	    loop(P, Board)
    end.

exec() ->
    speak:speak(['Version', '1']),
    Board = getBoard(),
    init(),
    calculate:start(),
    loop(#piece{}, Board).


atom_to_integer(A) ->
    list_to_integer(atom_to_list(A)).


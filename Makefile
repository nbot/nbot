.SUFFIXES: .erl .beam

.erl.beam:
	erlc -W $<

ERL = erl -boot start_clean

MODS = listen exec speak board calculate heuristic move

all: compile doc

compile: ${MODS:%=%.beam}

clean: clean-doc
	rm -rf *.beam erl_crash.dump

doc:
	$(MAKE) -C doc

clean-doc:
	$(MAKE) -C doc clean

%% ------------------------------------------------------------------------------
%% "THE BEER-WARE LICENSE" (Revision 42):
%% <meskio@sindominio.net> wrote this file. As long as you retain this notice you
%% can do whatever you want with this stuff. If we meet some day, and you think
%% this stuff is worth it, you can buy me a beer in return Ruben Pollan
%% ------------------------------------------------------------------------------


-record(piece, {
	num,
	type = null,
	new = false
    }).

-record(board, {
	height = 19,
	width = 10,
	pieces = []
    }).

-record(move, {
	rot = [[]],
	heuristic = -100,
	removedLines = 0,
	column
    }).

%% ------------------------------------------------------------------------------
%% "THE BEER-WARE LICENSE" (Revision 42):
%% <meskio@sindominio.net> wrote this file. As long as you retain this notice you
%% can do whatever you want with this stuff. If we meet some day, and you think
%% this stuff is worth it, you can buy me a beer in return Ruben Pollan
%% ------------------------------------------------------------------------------

-module(heuristic).
-export([calculate/0]).
-include_lib("records.hrl").


% Weight of each heuristic
%
% Every heuristic will return a number [0, 100], the global heuristic is calculated by
% H1*?W1 + ... + Hn*?Wn
-define(W1, 2).
-define(W2, 1).
-define(W3, 1.5).
-define(W4, 0.2).


calculate() ->
    receive
	{Pid, {#move{} = M, #board{height = Height} = B}} ->
	    PieceBoard = board:placePiece(B, M),
	    {H1, B1, NumLines} = heuLines(PieceBoard, B),
	    H2 = heuHeight(B1, Height),
	    H3 = heuHoles(B1),
	    H4 = heuCloseSides(M, B#board.width),
	    Heuristic = H1*?W1 + H2*?W2 + H3*?W3 + H4*?W4,
	    Pid ! {M#move{heuristic = Heuristic, removedLines = NumLines}, B1}
    end.

% heuristic based on the number of lines deleted by the piece.
%
% if the usage of the board is > 70% the heuristic is directly related with
% the number of lines removed
%
% in other case 0 is has bigger value than 1 line, so we promote to delete
% several lines together, that is better for multiplayer games
heuLines(PieceBoard, Board) ->
    {NumLines, NewPieceBoard} = linesDeleted(PieceBoard),
    case board:usage(Board) > 0.7 of
	true ->
	    {NumLines * 25, NewPieceBoard, NumLines};
	false ->
	    case NumLines of
		0 -> 
		    {10, NewPieceBoard, NumLines};
		1 -> 
		    {0, NewPieceBoard, NumLines};
		Other ->
		    {Other*25, NewPieceBoard, NumLines}
	    end
    end.

% in equal conditions as lower as is the piece is better
heuHeight(PieceBoard, BoardHeight) ->
    100 * (1 - (length(PieceBoard)/BoardHeight)).

% heuristic based on the holes lived below the piece
%
% the importance of each hole depends of how close it is from the piece
% the closest holes are taken more into account than the farther holes.
heuHoles(PieceBoard) -> 
    100 - numHoles(PieceBoard).

% heuristic based on how close is the piece to the sides of the board
% as closer better
heuCloseSides(#move{rot = [RotRow|_], column = Column}, Width) ->
    MoveColumn = Column + (length(RotRow)/2),
    case MoveColumn > Width/2 of
	true ->
	    100 * (1 - (Width - (Column+length(RotRow))) * 2/Width);
	false ->
	    100 * (1 - (Column*2/Width))
    end.


linesDeleted([]) -> {0, []};
linesDeleted([Row|Board]) ->
    case isLineFill(Row) of
	true -> 
	    {Num, B} = linesDeleted(Board),
	    {Num + 1, B};
	false ->
	    {Num, B} = linesDeleted(Board),
	    {Num,  [Row|B]}
    end.
isLineFill([]) -> true;
isLineFill(['0'|_]) -> false;
isLineFill([_|B]) -> isLineFill(B).

numHoles([]) ->
    0;
numHoles(Columns) ->
    [Row|_] = Columns,
    countHoles(Columns, [ false || _ <- Row], 0).
countHoles([], _, _) -> 0;
countHoles([Row|Rows], ValidCols, Line) ->
    {Num, NewValidCols} = holes(Row, ValidCols),
    case (6-Line)*4 of
	W when W > 0 ->
	    (Num * W) + countHoles(Rows, NewValidCols, Line+1);
	_ ->
	    0
    end.
holes([], _) -> {0, []};
holes(['0'|Cols], [true|ValidCols]) -> 
    {Num, NewValidCols} = holes(Cols, ValidCols),
    {1 + Num, [true|NewValidCols]};
holes([x|Cols], [false|ValidCols]) ->
    {Num, NewValidCols} = holes(Cols, ValidCols),
    {Num, [true|NewValidCols]};
holes([_|Cols], [Valid|ValidCols]) ->
    {Num, NewValidCols} = holes(Cols, ValidCols),
    {Num, [Valid|NewValidCols]}.

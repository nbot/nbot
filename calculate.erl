%% ------------------------------------------------------------------------------
%% "THE BEER-WARE LICENSE" (Revision 42):
%% <meskio@sindominio.net> wrote this file. As long as you retain this notice you
%% can do whatever you want with this stuff. If we meet some day, and you think
%% this stuff is worth it, you can buy me a beer in return Ruben Pollan
%% ------------------------------------------------------------------------------

-module(calculate).
-export([start/0, rpc/2]).
-include_lib("records.hrl").

start() ->
    Pid = spawn_link(fun loop/0),
    register(calculate, Pid).

rpc(Piece, #board{} = B) ->
    calculate ! {self(), {Piece, B}},
    receive
	{calculate, Response} ->
	    Response
    end.

loop() ->
    receive
	{Pid, {Piece, #board{} = Board}} ->
	    Ret = bestMove(Piece, Board),
	    Pid ! {calculate, Ret},
	    loop();
	_ ->
	    loop()
    end.

bestMove(P, B) ->
    RotList = board:genRot(P),
    sendRot(RotList, B),
    getBestRot(length(RotList), #move{}, []).

getBestRot(0, Move, Board) ->
    {Move, Board};
getBestRot(N, Move, Board) ->
    receive
	{#move{} = M, B} ->
	    case (M#move.heuristic > Move#move.heuristic) of
		true -> getBestRot(N-1, M, B);
		false -> getBestRot(N-1, Move, Board)
	    end
    end.

sendRot(RotList, Board) ->
    lists:map(
	fun(Rot) -> 
		Pid = spawn(fun sendPos/0),
		Pid ! {self(), {Rot, Board}}
	end,
	RotList).

sendPos() ->
    receive
	{Pid, {Piece, #board{} = Board}} ->
	    [R|_] = Piece,
	    NumPos = Board#board.width-length(R)+1,
	    genPos(NumPos, Piece, Board),
	    Pid ! getBestRot(NumPos, #move{}, [])
    end.

genPos(0, _, _) -> true;
genPos(N, Piece, Board) ->
    Move = #move{ rot = Piece, column = N-1 },
    Pid = spawn(fun heuristic:calculate/0),
    Pid ! {self(), {Move, Board}},
    genPos(N-1, Piece, Board).

%% ------------------------------------------------------------------------------
%% "THE BEER-WARE LICENSE" (Revision 42):
%% <meskio@sindominio.net> wrote this file. As long as you retain this notice you
%% can do whatever you want with this stuff. If we meet some day, and you think
%% this stuff is worth it, you can buy me a beer in return Ruben Pollan
%% ------------------------------------------------------------------------------

-module(board).
-export([gen/2, 
	modify/3,
	update/3,
	piecePos/1,
	usage/1,
        detectPiece/1,
        placePiece/2,
        split/3,
	genRot/1]).
-include_lib("records.hrl").

% Board is a list of Rows, the head is the upper row.
% Each row is a list of cols, they are atoms
% '0' for empty  >'0' for block.

%% @spec gen(int(), int()) -> Board.
gen(Height, Width) ->
    #board{
	height = Height,
	width = Width,
	pieces = gen__(Height, Width)
    }.

gen__(0, _) -> [];
gen__(Rows, Cols) ->
    [genRow(Cols) | gen__(Rows-1, Cols)].

genRow(0) -> [];
genRow(Cols) -> ['0'|genRow(Cols-1)].


%% @spec modify(Board, int(), [atom()]) -> Board.
modify(#board{height = H}, Row, _) when (Row >= H) or (Row < 0) ->
    throw({boardRowErr, "Row out of the board"});
modify(#board{height = H, pieces = B} = Board, Row, Cols) ->
    Board#board{ pieces = modify__(B, (H-1)-Row, Cols)}.

%% @spec update(Board, [[atom()]], int()) -> Board.
update(_, _, Num) when (Num < 0) ->
    throw({boardModifyErr, "Can not handle negative number of removed lines"});
update(#board{height = H, width = W, pieces = P} = Board, Update, NumLinesRem) ->
    Pieces = genEmptyRows(NumLinesRem, W) ++ nthhead(H-length(Update)-NumLinesRem, P) ++ Update,
    Board#board{ pieces =  Pieces }.
genEmptyRows(0, _) -> [];
genEmptyRows(Num, Cols) -> [genRow(Cols)|genEmptyRows(Num-1, Cols)].


modify__([R|Rows], 0, Cols) ->
    [modifyCols(R,Cols)|Rows];
modify__([R|Rows], N, Cols) ->
    [R|modify__(Rows, N-1, Cols)].

modifyCols([],C) -> C;
modifyCols(_,[]) -> [];
modifyCols([x|OldCols],[C|Cols]) when C =< '0' ->
    [x|modifyCols(OldCols, Cols)];
modifyCols([_|OldCols],[C|Cols]) when C < '0' ->
    ['0'|modifyCols(OldCols, Cols)];
modifyCols([_|OldCols],[C|Cols]) ->
    [C|modifyCols(OldCols, Cols)].

%% @spec piecePos([atom()]) -> int().
piecePos([]) -> 10;
piecePos([N|_]) when N > '0' ->
    0;
piecePos([_|Rows]) ->
    1 + piecePos(Rows).

%% @spec usage(Board) -> float().
usage(#board{height = H, pieces = P}) ->
    (H-firstRowUsed(P))/H.
firstRowUsed([]) -> 0;
firstRowUsed([Row|Pieces]) ->
    case isEmptyRow(Row) of
	true -> firstRowUsed(Pieces) + 1;
	false -> 0
    end.

isEmptyRow([]) -> true;
isEmptyRow([X|_]) when X > '0' -> false;
isEmptyRow([_|Row]) ->
    isEmptyRow(Row).

%% @spec detectPiece([atom()]) -> PieceType.
detectPiece(['-1'|_]) -> t;
detectPiece(['-2'|_]) -> i;
detectPiece(['-3'|_]) -> o;
detectPiece(['-4'|_]) -> l;
detectPiece(['-5'|_]) -> j;
detectPiece(['-6'|_]) -> s;
detectPiece(['-7'|_]) -> z;
detectPiece([_|Rows]) ->
    detectPiece(Rows);
detectPiece([]) -> null.


%% @spec placePiece(Board, Move) -> [[atom()]].
placePiece(#board{pieces = []}, _) ->
    throw({boardRowErr, "Trying to put piece out of board"});
placePiece(#board{pieces = Pieces, height = Height}, #move{rot = Rot, column = Column} = Move) ->
    [_|P] = Pieces,
    case checkCollision (P, Rot, Column) of
	true -> % in case of piece higher than the board won't be detected
	    putPiece(Rot, Pieces, Column);
	false ->
	    placePiece(#board{pieces = P, height = Height-1}, Move)
    end.

%% @spec checkCollision(PieceMatrix, Rotation, int()) -> bool().
checkCollision(_, [], _) ->
    false;
checkCollision([], _, _) ->
    true;
checkCollision([B1|Board], [P1|Piece], Column) ->
    Row = lists:nthtail(Column, B1),
    case checkCollisionRow(P1, Row) of
	true ->
	    true;
	false ->
	    checkCollision(Board, Piece, Column)
    end.
checkCollisionRow([], _) -> false;
checkCollisionRow(['x'|_], [P|_]) when P =/= '0' -> true;
checkCollisionRow([_|R], [_|P]) ->
    checkCollisionRow(R, P).

putPiece([],B,_) -> B;
putPiece(P,[],Col) ->
    throw({boardRowErr, "Trying to put piece out of board", P, Col});
putPiece([P1|P],[B1|B],Col) ->
    [putPieceCol(P1,B1,Col)|putPiece(P,B,Col)].
putPieceCol([],B,_) -> B;
putPieceCol([P1|P],[B1|B],Col) when Col == 0 ->
    case P1 =:= 'x' of
	true ->
	    [P1|putPieceCol(P,B,Col)];
	false ->
	    [B1|putPieceCol(P,B,Col)]
    end;
putPieceCol(P,[B1|B],Col) ->
    [B1|putPieceCol(P,B,Col-1)].



%% @spec split([[atom()]], int(), int()) -> [[atom()]].
split(Board, Col, Width) ->
    StartBoard = lists:map(fun(R) -> lists:nthtail(Col, R) end, Board),
    lists:map(fun(R) -> nthhead(Width, R) end, StartBoard).
nthhead(0, _) -> [];
nthhead(N, [E|List]) -> [E|nthhead(N-1, List)].


%% @spec genRot(PieceType) -> [[[atom()]]].
genRot(t) ->
    [[[x, x, x],
      [o, x, o]],
     [[x, o],
      [x, x],
      [x, o]],
     [[o, x, o],
      [x, x, x]],
     [[o, x],
      [x, x],
      [o, x]]];
genRot(i) ->
    [[[x, x, x, x]],
     [[x],
      [x],
      [x],
      [x]]];
genRot(o) ->
    [[[x, x],
      [x, x]]];
genRot(l) ->
    [[[x, x, x],
      [x, o, o]],
     [[x, o],
      [x, o],
      [x, x]],
     [[o, o, x],
      [x, x, x]],
     [[x, x],
      [o, x],
      [o, x]]];
genRot(j) ->
    [[[x, x, x],
      [o, o, x]],
     [[x, x],
      [x, o],
      [x, o]],
     [[x, o, o],
      [x, x, x]],
     [[o, x],
      [o, x],
      [x, x]]];
genRot(s) ->
    [[[o, x, x],
      [x, x, o]],
     [[x, o],
      [x, x],
      [o, x]]];
genRot(z) ->
    [[[x, x, o],
      [o, x, x]],
     [[o, x],
      [x, x],
      [x, o]]].

%% ------------------------------------------------------------------------------
%% "THE BEER-WARE LICENSE" (Revision 42):
%% <meskio@sindominio.net> wrote this file. As long as you retain this notice you
%% can do whatever you want with this stuff. If we meet some day, and you think
%% this stuff is worth it, you can buy me a beer in return Ruben Pollan
%% ------------------------------------------------------------------------------

-module(move).
-export([start/0, move/1]).
-include_lib("records.hrl").

start() ->
    Pid = spawn_link(fun loop/0),
    register(move, Pid).

move(Request) ->
    move ! Request.

loop() ->
    receive
	[#move{} = M, #piece{} = P] ->
	    io:format("Message ~w~w~w~n", [M#move.heuristic, M#move.rot, M#move.column]),
	    moveIt(M, P),
	    loop()
    end.

% move and drop the piece
moveIt(#move{rot = Rot, column = Col}, #piece{ type = PieceType, num = PieceNum }) ->
    moveRotate(Rot, board:genRot(PieceType), PieceNum),
    Pos = calculatePosition(PieceType, Rot, board:genRot(PieceType)),
    moveToPos(Col, Pos, PieceNum),
    speak:speak(['Drop', PieceNum]).


moveToPos(Dest, Pos, _) when Dest =:= Pos -> true;
moveToPos(Dest, Pos, PieceNum) when Dest < Pos ->
    speak:speak(['Left', PieceNum]),
    moveToPos(Dest, Pos-1, PieceNum);
moveToPos(Dest, Pos, PieceNum) ->
    speak:speak(['Right', PieceNum]),
    moveToPos(Dest, Pos+1, PieceNum).

moveRotate(R, [R|_], _) -> true;
moveRotate(Rot, [_|Moves], PieceNum) ->
    speak:speak(['Rotate', PieceNum]),
    moveRotate(Rot, Moves, PieceNum).


calculatePosition(T, R, [_,R|_]) when T =/= z -> 5;
calculatePosition(_, _, _) -> 4.

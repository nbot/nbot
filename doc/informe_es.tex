%% ------------------------------------------------------------------------------
%% "THE BEER-WARE LICENSE" (Revision 42):
%% <meskio@sindominio.net> wrote this file. As long as you retain this notice you
%% can do whatever you want with this stuff. If we meet some day, and you think
%% this stuff is worth it, you can buy me a beer in return Ruben Pollan
%% ------------------------------------------------------------------------------
\documentclass[12pt]{article}
\usepackage[fixlanguage]{babelbib}
\selectbiblanguage{spanish}
\usepackage[spanish]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\bibliographystyle{economet}

\title{ \vspace{-1cm} NBot: un robot de tetris}
\author{Ruben Pollan
\\ \small{Universidad de Zaragoza}
}

\date{\today}

\begin{document}
\maketitle
\begin{abstract}
{\small
NBot es una inteligencia artificial para jugar al tetris. Desarrollada en
erlang, usando su potencia en programación concurrente y paso de mensajes para
programarlo distribuido en varios procesos. Usando netris, una implementación de
tetris para terminales unix, y su api para comunicarse con robots.
}
\end{abstract}
\newpage


\section{Introducción}

El tetris es un videojuego con mas de 25 años de historia, uno de los
videojuegos mas conocidos. Su mecánica es muy simple: Distintos
tetrominos\footnote{Un tetrominó es una forma geométrica compuesta por cuatro 
cuadrados conectados ortogonalmente.} caen de la parte  superior de la pantalla.
El jugador no puede impedir esta caída pero puede decidir la rotación de la
pieza (0\textsuperscript{o}, 90\textsuperscript{o}, 180\textsuperscript{o},
270\textsuperscript{o}) y en qué lugar debe caer. Cuando una línea horizontal se
completa, esa línea desaparece y todas las piezas que están por encima
descienden una posición, liberando espacio de juego y por tanto facilitando la
tarea de situar nuevas piezas. La caída de las piezas se acelera
progresivamente. El juego acaba cuando las piezas se amontonan hasta salir del
área de juego.

Una de sus implementaciones mas extendida en el mundo unix es
netris\footnote{http://www.netris.org/}, una variante de tetris para terminales
unix con soporte para juegos en red entre dos jugadores. En caso de jugar en red
cada jugador juega con su tablero independiente recibiendo las mismas piezas que
el otro jugador. Cada vez que un jugador elimina dos o mas lineas de la pantalla
a su oponente le aparecen lineas con huecos por debajo del tablero, tantas como
lineas ha eliminado menos una.

Netris tiene una interfaz para programar robots y viene con un robot de ejemplo
programado en C. El robot se comunica con netris a través de su entrada y salida
estándar. Netris le envia al robot actualizaciones del tablero y el robot escribe
por salida estándar las acciones que quiere
realizar\footnote{http://gitorious.org/nbot/nbot/blobs/raw/master/doc/robot\_desc}.

Hace tiempo que quería aprender erlang\footnote{http://www.erlang.org/}, me
llama la atención su facilidad para la programación concurrente y el ser un
lenguaje funcional con un aire a prolog.  Para asimilar un lenguaje de
programación nuevo opino que hace falta leer código de otros en ese lenguaje y
construir un programa (al menos unos pocos cientos de lineas) en ese lenguaje.
Para hacer lo segundó me planteé escribir un robot para netris, y así de paso
ver si todas las horas perdidas jugando al tetris pueden servir de algo.

Por lo tanto NBot hereda toda mi inexperiencia escribiendo programas en erlang.
Seguramente no sea un buen ejemplo de como se debe programar en erlang. Pero ha
sido muy divertido programarlo y racionalizar el como juego al tetris.

\section{Como se usa NBot}

Para usar NBot hace falta tener instalado erlang y netris. Con erlang instalado
compilamos NBot en la shell:
\begin{verbatim}
$ make
\end{verbatim}
Y ya podemos verlo funcionar:
\begin{verbatim}
$ netris -r ./nbot
\end{verbatim}

Podemos jugar contra NBot o poner otro robot a jugar contra el. Para ello
lanzamos NBot en una shell en modo servidor:
\begin{verbatim}
$ netris -w -r ./nbot
\end{verbatim}
Y nosotros en otra shell lanzamos otro netris en modo cliente conectándonos 
por red a el:
\begin{verbatim}
$ netris -c localhost
\end{verbatim}
O lanzamos otro robot a competir con el:
\begin{verbatim}
$ netris -c localhost -r netris-sample-robot
\end{verbatim}

La última versión de NBot se puede encontrar en su repositorio publico:
http://gitorious.org/nbot/

\section{Diseño}

NBot hace un gran uso del soporte para concurrencia de erlang, y de su interfaz
para paso de mensajes entre procesos. Durante una ejecución de NBot suele haber
los siguientes procesos ejecutándose:
\begin{itemize}
\item {\bf listen}. Se encarga de escuchar en la entrada estándar los comandos
enviados desde netris y pasarselos a {\em exec}.
\item {\bf speak}. Recibe comandos y se los pasa a través de la salida estándar
a netris.
\item {\bf exec}. Interpreta los comandos enviados por listen y ejecuta lo
necesario: mantiene actualizado el tablero, invoca {\em calculate} para que
devuelva el mejor movimiento, le pide a {\em move} que mueva la pieza a su
sitio, ...
\item {\bf calculate}. Espera información de nuevas piezas para calcular el
movimiento optimo para esta pieza. Se encarga de lanzar tantos procesos {\em
heuristic} como posibles colocaciones de pieza y comunicarse con ellos. 
Devuelve el mejor de los movimientos a {\em exec}.
\item {\bf heuristic}. Calcula la heurística de un movimiento dado (descrita en
profundidad en el apartado~\ref{sec:heuristica}). Para cada nueva pieza hay en 
ejecución un proceso {\em heuristic} por cada posible movimiento.
\item {\bf move}. Se encarga de rotar y posicionar la pieza en el lugar elegido.
\end{itemize}

El código esta organizado en diferentes módulos, uno por cada proceso descrito
hasta ahora y ademas {\bf board} un conjunto de funciones orientadas a
actualizar y extraer información del tablero.

\subsection{Heurística}
\label{sec:heuristica}

El modulo donde reside realmente la inteligencia del robot es {\bf heuristic}.
Este modulo calcula la heurística de un movimiento dado, una valoración numérica
de como de bueno es ese movimiento, cuanto mayor mejor es el movimiento.

La heurística se calcula a través de la suma ponderada de 4 heurísticas:
\[
heuristica = \sum_{i=1}^{4} H_{i} \times W_{i}
\]
Donde $W_{i}$ son las ponderaciones de cada heurística y $H_{i}$ es la
heurística que valora una característica concreta del movimiento. Los valores de
$H_{i}$ son números positivos entre 0 y 100, excepto {\em huecos} que puede tomar
en algún caso valores negativos.

Las 4 características que se miden a través de las heurísticas son:
\begin{itemize}
\item {\bf lineas}. Valora el número de lineas que este movimiento va a eliminar
de tablero. NBot esta pensado para jugar contra otros jugadores, por lo que
intenta siempre que puede eliminar mas de una linea a la vez, pasa así añadirle
lineas al oponente.

El valor de la heurística depende de la ocupación del tablero. Si la altura
máxima ocupada es inferior al 70\% de la altura del tablero la heurística será
mayor para los movimientos que no eliminen ninguna linea que para los que
eliminan una. En caso contrario, si el tablero esta muy lleno, se potencia mas
la eliminación de lineas a el competir con el otro jugador, por lo que la
heurística es mayor cuantas mas lineas se eliminen.
\[
H_{1} = \left\{
\begin{array}{l l}
\text{Si ocupación} <= 70\% & \left\{
    \begin{array}{l l}
    \text{Si no se eliminan linea}s & 10 \\
    \text{Si se elimina 1 linea} & 0 \\
    \text{En otro caso} & lineas\_eliminadas \times 25 \\
    \end{array} \right. \\ \\
\text{Si ocupación} > 70\% & lineas\_eliminadas \times 25
\end{array} \right.
\]

\item {\bf altura}. Cuanto mas baja sea la posición de la pieza en el tablero
mejor:
\[
H_{2} = 100 \times (1 - \frac{posicion\_pieza}{altura\_tablero})
\]

\item {\bf huecos}. Calcula el numero de huecos sin rellenar que la pieza va a
dejar debajo de ella. La importancia de los huecos depende del numero de lineas
de distancia entre el hueco y la pieza. Cuanto mas lejos menos importancia,
hasta que mas allá de 6 lineas no se tienen en cuenta:
\[
H_{3} = 100 - \sum(6-lineas\_lejos) \times 4
\]

Los valores de esta heurística pueden alcanzar números negativos, en caso de que
la pieza deje muchos huecos debajo de ella.

\item {\bf cercanía a los lados}. Es mejor  cuanto mas cerca se coloquen las
piezas a los lados, deja el tablero con mas espacio para maniobrar enmedio.
\[
H_{4} = 100 \times (1 - \frac{distancia\_lateral}{\frac{ancho\_tablero}{2}})
\]
\end{itemize}

Las ponderaciones para cada una de las heurísticas se han calculado de forma
experimental. Partiendo de una configuración creada por intuición se han ido
modificando sus valores observando como cada una las heurísticas afecta al
juego. La mejor configuración que se ha encontrado es:
\[
W_{1} = 2, W_{2} = 1, W_{3} = 1.5, W_{4} = 0.2
\]


\bibliography{default}
\begin{thebibliography}{}

\bibitem{armstrong07}
  Joe Armstrong,
  Programming Erlang.
  Pragmatic Bookshelf,
  2007.

\bibitem{cesarini09}
  Francesco Cesarini, Simon Thompson,
  Erlang Programming.
  O'Reilly Media,
  2009.

\bibitem{erlang}
  http://www.erlang.org/doc/

\bibitem{wikipedia}
  http://es.wikipedia.org/wiki/Tetris

\end{thebibliography}

\end{document}

%% ------------------------------------------------------------------------------
%% "THE BEER-WARE LICENSE" (Revision 42):
%% <meskio@sindominio.net> wrote this file. As long as you retain this notice you
%% can do whatever you want with this stuff. If we meet some day, and you think
%% this stuff is worth it, you can buy me a beer in return Ruben Pollan
%% ------------------------------------------------------------------------------

-module(speak).
-export([start/0, speak/1]).

start() ->
    register(speak, spawn_link(fun loop/0)).

speak(Request) ->
    speak ! Request.

loop() ->
    receive
	Msg ->
	    List = lists:map(fun erlang:atom_to_list/1, Msg),
	    Str = string:join(List, " "),
	    io:format("~s~n",[Str]),
	    loop()
    end.

%% ------------------------------------------------------------------------------
%% "THE BEER-WARE LICENSE" (Revision 42):
%% <meskio@sindominio.net> wrote this file. As long as you retain this notice you
%% can do whatever you want with this stuff. If we meet some day, and you think
%% this stuff is worth it, you can buy me a beer in return Ruben Pollan
%% ------------------------------------------------------------------------------

-module(listen).
-export([listen/0]).

%% @spec get_command() -> [atom()].
get_command() ->
    Line = io:get_line(""),
    Tokens = string:tokens(Line, " \n"),
    lists:map(fun list_to_atom/1, Tokens).

%% @spec listen() -> none().
listen() ->
    C = get_command(),
    exec:exec(C),
    listen().
